PACKAGE_NAME = goodreads
PROJECT_DIR = $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

BOLD := \033[1m
RESET := \033[0m

PC = python

build-data-path:
	@echo
	bash -c "rm ./$(PACKAGE_NAME)/datapath.py ||:"
	bash -c "echo DATAPATH = \'$(PWD)/data/\' >> ./$(PACKAGE_NAME)/datapath.py"

install: install-python-prerequisites install-python

install-python-prerequisites:
	@echo
	@echo "$(BOLD)Installing/upgrading prerequisites in base environment ($(PC))...$(RESET)"
	@$(PC) -m pip install -U --no-cache-dir pip setuptools
	@echo "$(BOLD)Done...$(RESET) (installed: pip and setuptools)"

install-python:
	@echo
	@echo "$(BOLD)Installing $(PACKAGE_NAME) in base environment ($(PC))...$(RESET)"
	@$(PC) -m pip install -U --no-cache-dir .
	@echo "$(BOLD)Done...$(RESET) (installed: $(PACKAGE_NAME))"

all: build-data-path install

clean:
	@echo
	@echo "$(BOLD)Cleaning...$(RESET)"
	bash -c "git clean -f -xdf"
	@echo "$(BOLD)Done...$(RESET)"
