import os

from goodreads.datapath import DATAPATH
from goodreads.util import load_bank


class Carer:

    def __init__(self):
        self.bankname = DATAPATH
        self.bank = load_bank(self.bankname)

    def remove_entries(self, *argv):
        for p in argv:
            try:
                n = p['id']
            except (KeyError, TypeError):
                continue
            n = str(n).split('abs/')[1].split('v')[0]
            fn = n + '.json'
            try:
                os.remove(self.bankname + fn)
                print('Removed: ' + n)
            except FileNotFoundError:
                print(n + ' not present in data bank anyway.')
            self.bank = load_bank(self.bankname)
