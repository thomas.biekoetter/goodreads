import urllib.request
import feedparser
import json

from goodreads.datapath import DATAPATH
from goodreads.util import load_bank


class Fetcher:

    def __init__(self):
        self.bankname = DATAPATH
        self.rq = "http://export.arxiv.org/api/query?search_query=id:"
        self.bank = load_bank(self.bankname)

    def fetch_by_number(self, num):
        if not self.is_present(num):
            try:
                url = self.rq + str(num)
                with urllib.request.urlopen(url) as req:
                    data = req.read()
                res = dict(feedparser.parse(data)['entries'][0])
                with open(self.bankname + str(num) + '.json', 'w') as f:
                    json.dump(res, f, indent=4)
                    print('Paper saved to bank')
                self.bank = load_bank(self.bankname)
            except:
                print('Something went wrong.')
                raise
        else:
            print('Paper already saved in bank.')

    def is_present(self, num):
        num = str(num)
        for x in self.bank:
            y = x['id'].split('/')[-1].split('v')[0]
            if y == num:
                res = True
                break
        else:
            res = False
        return res

