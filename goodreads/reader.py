from goodreads.datapath import DATAPATH
from goodreads.util import load_bank
from goodreads.util import print_nicely


class Reader:

    def __init__(self):
        self.bankname = DATAPATH
        self.bank = load_bank(self.bankname)

    def find_by_keywords(self, *argv):
        ws = []
        for word in argv:
            try:
                w = str(word)
                ws.append(w.lower())
            except:
                pass
        print('Looking for papers containing:')
        print('    ', ws)
        print()
        print('=================================================')
        hits = []
        lw = len(ws)
        for p in self.bank:
            used = []
            h = 0
            for v in p.values():
                for w in ws:
                    if not w in used:
                        vs = str(v).lower()
                        if w in vs:
                            h += 1
                            used.append(w)
                            break
            if h == lw:
                hits.append(p)
        print_nicely(hits)
        print('=================================================')
        return hits
