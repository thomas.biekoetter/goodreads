import os
import json
from json.decoder import JSONDecodeError


def load_bank(name):
    bank = []
    for fn in os.listdir(name):
        if not fn == '.gitignore':
            with open(name + fn, 'r') as f:
                try:
                    js = json.load(f)
                except JSONDecodeError:
                    raise
            bank.append(js)
    return bank

def print_nicely(ps):
    for p in ps:
        print()
        x = ''
        try:
            t = p['title_detail']['value'][0:80].replace('\n', '') + '...'
        except IndexError:
            t = p['title_detail']['value'].replace('\n', '') + '...'
        x += t + '\n'
        for a in p['authors']:
            x += a['name']
            x += ', '
        x += '  '
        x += p['published'].split('T')[0]
        x += '\n'
        x += p['id']
        print(x)
        print()
