from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name = "goodreads",
    version = "1.0.0",
    description = "A bucket list for arXiv papers.",
    long_description = long_description,
    packages = ["goodreads"],
    author = ["Thomas Biekotter"],
    author_email = [
        "thomas.biekoetter@desy.de"],
    url = "https://gitlab.com/thomas.biekoetter/goodreads",
    include_package_data = True,
    package_data = {"goodreads": ["*"]},
    install_requires = ["feedparser"],
    python_requires = '>=3.6'
)
